FROM ubuntu:latest
SHELL ["/bin/bash", "--login", "-i", "-c"]

RUN apt-get update
RUN apt-get dist-upgrade -y
RUN apt-get install curl -y

RUN touch ~/.bashrc && chmod +x ~/.bashrc
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
RUN . ~/.nvm/nvm.sh && source ~/.bashrc && nvm install node

ENV NODE_VERSION $(find . -name npm | grep "./root/.nvm/versions/node/v" | head -1 | cut -d "/" -f 6)
ENV node ./root/.nvm/versions/node/$NODE_VERSION/bin/node
ENV npm ./root/.nvm/versions/node/$NODE_VERSION/bin/npm
RUN npm i -g yarn

CMD cd /app && yarn install && yarn start
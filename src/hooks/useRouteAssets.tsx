import {useLocation} from "wouter";
import lines from "../pages/Contact/images/lines.svg";
import React from "react";
import clouds from "../pages/Home/images/clouds.svg";
import wave from "../pages/Home/images/wave.svg";
import dragon from "../pages/Skills/images/dragon.svg";
import {UseRouteAssests} from "./useRouteAssests";

function useRouteAssets(): UseRouteAssests {
    const [location] = useLocation();
    switch (location) {
        case '/contact':
            return {
                text: 'text-light-purple', white: 'text-sky-purple', bg: 'bg-dark-purple', location, img:
                    <div
                        className='fixed left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2 scale-150 w-full h-full overflow-hidden grid items-center'>
                        <img className="w-full  h-screen p-10 " src={lines} alt=""/>
                    </div>
            }
        case '/skills':
            return {
                text: 'text-light-red', white: 'text-sky-red', bg: 'bg-dark-red', location, img:
                    <div className='fixed w-screen h-screen  top-0 animate-rotate-3s'>
                        <img className="w-screen h-screen  " src={dragon} alt=""/>
                    </div>
            }
        case '/':
            return {
                text: 'text-light-blue', white: 'text-sky-blue', bg: 'bg-dark-blue', location, img:
                    <>
                        <div className='absolute bottom-0 mb-[70vh] left-1/2 transform -translate-x-1/2 w-full min-w-[1000px] mt-24'>
                            <img className="w-full" src={clouds} alt=""/>
                        </div>
                        <div className='absolute bottom-0  w-full '>
                            <img className="w-full" src={wave} alt=""/>
                        </div>
                        <div className='absolute bottom-0  h-[1000vh] w-screen bg-sky-blue transform translate-y-full '></div>
                    </>
            }
        default:
            return {text: 'text-white', white: 'text-white', bg: 'bg-transparent', location, img: <div></div>}
    }
}

export default useRouteAssets;
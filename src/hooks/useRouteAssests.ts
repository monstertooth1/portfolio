export interface UseRouteAssests{
    text: string,
    bg: string,
    white: string,
    location: string,
    img: JSX.Element
}
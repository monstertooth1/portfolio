import List from "../pages/Skills/components/List";
import Colored from "../pages/components/typography/Colored";
import {v4} from "uuid";
import ListItem from "../pages/Skills/components/ListItem";
import React from "react";

function useDataLooper<T>(data : object, callback:(key:string,value:T)=>JSX.Element) {
    const lists = [];
    for (const [key, value] of Object.entries(data)){
        lists.push(callback(key, value));
    }
    return lists;
}

export default useDataLooper;
import Home from "./pages/Home";
import {Route, Switch, useLocation} from "wouter";
import Skills from "./pages/Skills";
import Contact from "./pages/Contact";
import Page from "./pages/components/layouts/Page";
import {AnimatePresence, motion} from "framer-motion";

function App() {
    const locatoin = useLocation();
    return <Page className='relative'>
        <Switch>
            <Route path={'/contact'}>
                <AnimatePresence exitBeforeEnter={true}>
                    <motion.div
                        key={locatoin[0]}
                        transition={{duration: 1}}
                        initial={{opacity: 0}}
                        animate={{opacity: 1}}
                        exit={{opacity: 0}}>
                        <Contact/>
                    </motion.div>
                </AnimatePresence>
            </Route>
            <Route path={'/skills'}>
                <AnimatePresence exitBeforeEnter={true}>
                    <motion.div
                        key={locatoin[0]}
                        transition={{duration: 1}}
                        initial={{opacity: 0}}
                        animate={{opacity: 1}}
                        exit={{rotate: 360,opacity: 0}}>
                        <Skills/>
                    </motion.div>
                </AnimatePresence>
            </Route>
            <Route path={'/'}>
                <AnimatePresence exitBeforeEnter={true}>
                    <motion.div
                        key={locatoin[0]}
                        transition={{duration: 1}}
                        initial={{opacity: 0}}
                        animate={{opacity: 1}}
                        exit={{translateY: -1000, opacity: 0}}>
                        <Home/>
                    </motion.div>
                </AnimatePresence>
            </Route>
        </Switch>
    </Page>
}

export default App;

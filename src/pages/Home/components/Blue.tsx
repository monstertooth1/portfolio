import {BlueProps} from "./Blue.types";

export default function Blue({children} : BlueProps) {
    return <span className="text-light-blue">{children}</span>
}
import {SpikeProps} from "./Spike.types";
import triangle from "../images/triangle.svg";

export default function Spike({className} : SpikeProps) {
    return <div className={`transform   ${className}`}>
        <img className="w-full" src={triangle} alt=""/>
    </div>
}
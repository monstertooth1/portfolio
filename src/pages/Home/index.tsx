import dragon from "./images/dragon.svg";
import Flexcol from "../components/layouts/Flexcol";
import PagePadding from "../components/layouts/PagePadding";
import Colored from "../components/typography/Colored";
import SpinLink from "../components/SpinLink";

export default function Home({}) {
    return <PagePadding className="flex flex-col justify-center relative h-full text-center ">
        <Flexcol className="items-center  gap-0 my-auto pb-36">
            <Flexcol className="items-center gap-10">
                <Flexcol className="items-center text-center">
                    <Colored color="text-light-blue" className='text-36px md:text-48px'>2022</Colored>
                    <img src={dragon} alt=""/>
                </Flexcol>
                <Flexcol className="items-center gap-5 md:gap-0">
                    <Colored color="text-light-blue" className='text-48px md:text-72px'>KRISTUPAS KOVECKIS</Colored>
                    <Colored color="text-light-blue" className='text-36px'>WEB DEVELOPER FULL STACK</Colored>
                </Flexcol>
            </Flexcol>
            <SpinLink className='my-32'
                      balls={[{height: 'h-44', size: 'w-5 h-5'}, {height: 'h-64', size: 'w-2.5 h-2.5'}]}
                      ball='hover:bg-light-blue' text='hover:text-light-blue' href='/skills'>SKILLS</SpinLink>
        </Flexcol>
    </PagePadding>
}
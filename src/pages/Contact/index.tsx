import React from 'react';
import PagePadding from "../components/layouts/PagePadding";
import Flexcol from "../components/layouts/Flexcol";
import {Link} from "wouter";
import Colored from "../components/typography/Colored";
import leftEar from "../Contact/images/leftEar.svg";
import rightEar from "../Contact/images/rightEar.svg";
import Paw from "./components/Paw";
import SpinLink from "../components/SpinLink";

function Contact() {
    return <PagePadding className="flex flex-col justify-center relative h-full text-center ">
        <Flexcol className="items-center gap-20 my-auto ">
            <Flexcol className='2xl:gap-24'>
                <Colored color="text-light-purple" className='text-72px  2xl:ml-44'>Contact</Colored>
                <Flexcol className='2xl:flex-row 2xl:items-center gap-12'>
                    <Flexcol className="items-center gap-0 font-insomnia font-medium transform 2xl:order-2">
                        <div className="flex gap-x-16 md:gap-x-60">
                            <img className='h-24 md:h-auto' src={leftEar} alt=""/>
                            <img className='h-24 md:h-auto' src={rightEar} alt=""/>
                        </div>
                        <div className=' w-[200px] h-[180px] md:w-[365px] md:h-[326px]  bg-light-purple rounded-full md:text-96px text-48px grid items-center pt-10 text-shadow-purple'>
                            <div className='flex flex-col leading-13 md:leading-16'>
                                <div className='flex justify-center gap-x-16 md:gap-x-28 '>
                                    <div className='transform scale-x-200'>U</div>
                                    <div className='transform scale-x-200'>U</div>
                                </div>
                                <div className='transform scale-x-200'>W</div>
                            </div>
                        </div>
                    </Flexcol>
                    <Paw className='2xl:order-1'>Monstertooth1@gmail.com</Paw>
                    <Paw className='2xl:order-3'>+370 692 77 524</Paw>
                </Flexcol>
            </Flexcol>
            <SpinLink className='2xl:ml-48 my-32' balls={[{height: 'h-44', size: 'w-5 h-5'}, {height: 'h-64', size: 'w-2.5 h-2.5'}]} ball='hover:bg-light-purple' text='hover:text-light-purple' href='/'>HOME</SpinLink>
        </Flexcol>
    </PagePadding>
}

export default Contact;
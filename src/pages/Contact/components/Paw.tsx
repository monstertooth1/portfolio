import React from 'react';
import paw from "../images/paw.svg";
import Flexcol from "../../components/layouts/Flexcol";
import {PawProps} from "./Paw.types";

function Paw({children, className}:PawProps) {
    return <Flexcol className={'items-center '+className}>
        <img className='w-44 md:w-80 transform translate-y-1 mx-5' src={paw} alt=""/>
        <div className='bg-light-purple rounded-2xl pb-1 pt-2 px-4 text-18px relative z-10 w-full md:text-36px whitespace-nowrap'>{children}</div>
    </Flexcol>
}

export default Paw;
import React from 'react';
import {RedProps} from "./Red.types";

function Red({children} : RedProps) {
    return <span className="text-light-red">{children}</span>
}

export default Red;
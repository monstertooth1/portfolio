import React from 'react';
import triangle from "../images/triangle.svg";
import banner from "../images/banner.svg";
import {ListItemProps} from "./ListItem.types";

function ListItem({children} : ListItemProps) {
    return (
        <div className="bg-light-red relative px-2 py-1 flex gap-2 mr-6">
            <img className="shrink-0" src={triangle} alt=""/>
            <div className="whitespace-nowrap">{children}</div>
            <img className="absolute right-0 top-1/2 transform -translate-y-1/2 mr-1 translate-x-full h-full" src={banner} alt=""/>
        </div>
    );
}

export default ListItem;
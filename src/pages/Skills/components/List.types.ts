export interface ListProps{
    children: JSX.Element | JSX.Element[],
    title: JSX.Element | JSX.Element[],
}
import React from 'react';
import Colored from "../../components/typography/Colored";
import {v4} from "uuid";
import ListItem from "./ListItem";
import List from "./List";
import {ListsProps} from "./Lists.types";

function Lists({title, skills}:ListsProps) {
    return <List title={<Colored color="text-light-red" >{title}</Colored>}>
        {skills.map(skill=><ListItem key={v4()}>{skill}</ListItem>)}
    </List>;
}

export default Lists;
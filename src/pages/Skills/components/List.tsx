import React from 'react';
import {ListProps} from "./List.types";

function List({children, title} : ListProps) {
    return (
        <div>
            <div className="flex flex-col gap-2">
                <div className="text-24px">
                    {title}
                </div>
                <div className="flex flex-col gap-1">
                    {children}
                </div>
            </div>
        </div>
    );
}

export default List;
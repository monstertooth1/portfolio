import Page from "../components/layouts/Page";
import PagePadding from "../components/layouts/PagePadding";
import Nav from "../components/Nav";
import hornLeft from './images/hornLeft.svg';
import hornRight from './images/hornRight.svg';
import dragon from './images/dragon.svg';
import spike from './images/spike.svg';
import List from "./components/List";
import ListItem from "./components/ListItem";
import React from "react";
import Colored from "../components/typography/Colored";
import {v4} from "uuid";
import useDataLooper from "../../hooks/useDataLooper";
import Lists from "./components/Lists";
import {Link} from "wouter";
import Flexcol from "../components/layouts/Flexcol";
import SpinLink from "../components/SpinLink";

const data = {
    'PROGRAMMING LANGUAGES': ['TYPESCRIPT', 'JAVASCRIPT', 'C++', 'C#', 'PHP', 'SASS', 'HTML', 'JAVA'],
    'GRAPHIC DESIGN': ['FIGMA', 'PHOTOSHOP', 'BLENDER', 'ADOBE PREMIERE', 'ADOBE AFTER EFFECTS', 'MAYA', '3DMAX'],
    'LIBRARIES': ['REACT', 'REDUX TOOLKIT', 'TOOLKIT', 'VUE', 'BABEL'],
    'HOSTING': ['NGINX', 'APACHE', 'HEROKU', 'AMAZON AWS'],
    'FRAMEWORKS': ['NODEJS', 'LARAVEL', 'WEBPACK', 'NEXTJS'],
    'TOOLS': ['VSCODE', 'GIT', 'DOCKER', 'NPM'],
    'FRONT-END FRAMEWORKS': ['TAILWINDCSS', 'BOOTSTRAP', 'MUI'],
    'GUIS': ['WXWIDGETS', 'QT', 'ELECTRON'],
    'GRAPHICAL LIBRARIES': ['OPENGL', 'DIRECTX'],
    'OPERATING SYSTEMS': ['LINUX', 'WINDOWS'],
    'GAME DEVELOPMENT': ['UNREAL ENGINE 4'],
};

function Skills() {
    const lists = useDataLooper<[]>(data, (key, value) => <Lists key={v4()} title={key} skills={value}/>);

    return <PagePadding className="grid grid-rows-auto-1fr  ">
        <Flexcol className='mt-5 gap-20'>
            <Flexcol className='gap-20'>
                <Flexcol className='gap-5'>
                    <div className="flex justify-center md:justify-start">
                        <img className="h-24" src={hornLeft} alt=""/>
                        <Colored color="text-light-red" className='text-48px md:text-72px md:mt-5 mt-10'>SKILLS</Colored>
                        <img className="h-24" src={hornRight} alt=""/>
                    </div>
                    <div>
                        <div className="transform translate-y-3 flex justify-between overflow-hidden">
                            {Array(100).fill(0).map((index) => <img key={v4()} src={spike} alt=""/>)}
                        </div>
                        <div className="bg-light-red h-3 rounded-xl"></div>
                    </div>
                </Flexcol>
                <div className="grid sm:flex sm:justify-between flex-wrap gap-10 justify-center overflow-hidden">
                    {lists}
                </div>
            </Flexcol>
            <div className='flex justify-center mb-6'>
                <SpinLink className='my-32' balls={[{height: 'h-64', size: 'w-5 h-5'}, {height: 'h-80', size: 'w-2.5 h-2.5'}]} ball='hover:bg-light-red' text='hover:text-light-red' href='/contact'>CONTACT</SpinLink>
            </div>
        </Flexcol>
    </PagePadding>
}

export default Skills;
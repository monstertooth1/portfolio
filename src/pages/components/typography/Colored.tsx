import React, {useEffect} from 'react';
import {ColoredProps} from "./Colored.types";
import {v4} from "uuid";
import useRouteAssets from "../../../hooks/useRouteAssets";

function forEachWord(words:string,callback:(word:string, index:number)=>void){
    return words.split(' ').map(callback);
}

function Colored({children, color, className = ''}: ColoredProps) {
    const assets = useRouteAssets();

    const text = forEachWord(children, (word, index) =>{

        const firstLetter = word.substring(0, 1);
        const restOfTheWord = word.substring(1);
        return <div key={v4()}>
            <span className={` ${color ?? assets.text}`}
            >{firstLetter}</span>{restOfTheWord}&nbsp;
        </div>
    });
    return (
        <div className={'flex flex-wrap justify-center '+className}>
            {text}
        </div>
    );
}

export default Colored;
export interface ColoredProps{
    children: string,
    color?: string,
    className?: string
}
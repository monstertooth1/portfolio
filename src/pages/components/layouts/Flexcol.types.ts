export interface FlexcolProps{
    children: JSX.Element | JSX.Element[],
    className?: string
}
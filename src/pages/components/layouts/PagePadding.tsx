import {PagePaddingProps} from "./PagePadding.types";

export default function PagePadding({children, className} : PagePaddingProps) {
    return <div className={` px-5 py-4 ${className}`}>
        {children}
    </div>
}
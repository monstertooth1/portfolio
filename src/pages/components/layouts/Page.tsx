import {PageProps} from "./Page.types";
import Nav from "../Nav";
import useRouteAssets from "../../../hooks/useRouteAssets";
import {AnimatePresence} from "framer-motion";
import {motion} from "framer-motion";
import {Route, Switch} from "wouter";

export default function Page({children, className = ''} : PageProps) {
    const assests = useRouteAssets();
    return <div className={`min-h-screen grid grid-rows-1 font-lucky overflow-hidden text-white transition duration-page-flip delay-page-flip ${assests.white} ${assests.bg} ${className}`}>
        <div className='z-20 grid grid-rows-auto-1fr'>
            <Nav/>
            {children}
        </div>
        <AnimatePresence  exitBeforeEnter={true}>
            <Switch>
                <Route path={'/contact'}>
                    <AnimatePresence exitBeforeEnter={true}>
                        <motion.div
                            key={assests.location}
                            transition={{duration: 1}}
                            initial={{opacity: 0}}
                            animate={{opacity: 1}}
                            exit={{opacity: 0}}
                        >
                            {assests.img}
                        </motion.div>
                    </AnimatePresence>
                </Route>
                <Route path={'/skills'}>
                    <AnimatePresence exitBeforeEnter={true}>
                        <motion.div
                            key={assests.location}
                            transition={{duration: 1}}
                            initial={{opacity: 0}}
                            animate={{opacity: 1}}
                            exit={{opacity: 0}}
                        >
                            {assests.img}
                        </motion.div>
                    </AnimatePresence>
                </Route>
                <Route path={'/'}>
                    <AnimatePresence exitBeforeEnter={true}>
                        <motion.div
                            key={assests.location}
                            transition={{duration: 1}}
                            initial={{opacity: 0}}
                            animate={{opacity: 1}}
                            exit={{translateY: -1000, opacity: 0}}
                        >
                            {assests.img}
                        </motion.div>
                    </AnimatePresence>
                </Route>
            </Switch>



        </AnimatePresence>
    </div>
}
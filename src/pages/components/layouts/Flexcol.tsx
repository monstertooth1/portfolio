import {FlexcolProps} from "./Flexcol.types";

export default function Flexcol({children, className = ''} : FlexcolProps) {
    return <div className={`flex flex-col ${className}`}>
        {children}
    </div>
}
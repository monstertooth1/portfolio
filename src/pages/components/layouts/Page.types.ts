export interface PageProps{
    children: JSX.Element | JSX.Element[],
    className: string,
    bg?: JSX.Element | JSX.Element[]
}
export interface PagePaddingProps{
    children: JSX.Element | JSX.Element[],
    className?: string
}
import React from "react";
import {SpinProps} from "./Spin.types";

function Spin({stick, ball, color}:SpinProps) {
    return <div className="absolute transform top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 pointer-events-none">
        <div className={`${stick}`}>
            <div className={`${ball} ${color} rounded-full bg-white transition-all transform hover:scale-200 pointer-events-auto`}></div>
        </div>
    </div>
}

export default Spin;
export interface SpinLinkProps{
    children: string,
    ball: string,
    text: string,
    href: string,
    className?: string,
    balls: Balls[]
}

export interface Balls{
    size: string,
    height: string
}
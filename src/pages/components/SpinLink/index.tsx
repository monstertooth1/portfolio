import React from 'react';
import {Link} from "wouter";
import {SpinLinkProps, Balls} from "./index.types";
import Spin from "./components/Spin";
import {v4} from "uuid";


function SpinLink({children, ball, text, href, balls, className}:SpinLinkProps){
    return (
        <div className={`relative ${className}`}>
            {balls.map(({size, height}:Balls)=><Spin key={v4()} stick={`${height} animate-rotate-2s`} ball={`${size}`} color={ball}/>)}
            <div className={`animate-pulse transition hover:animate-none transform hover:scale-105 ${text}`}>
                <Link className='text-36px' href={href}>{children}</Link>
            </div>
        </div>
    );
}

export default SpinLink;
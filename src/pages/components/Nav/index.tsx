import {MenuIcon} from "@heroicons/react/solid";
import MenuItem from "./components/MenuItem";
import {useState} from "react";
import useRouteAssets from "../../../hooks/useRouteAssets";

export default function Nav() {
    const [open, setOpen] = useState(false);
    const assets = useRouteAssets();

    return <nav className={`text-24px flex  items-center flex-col  w-full md:h-auto md:flex-row  md:bg-transparent  z-20 ${open ? assets.bg+' h-screen' : ''} p-5`}>
        <div className='flex justify-between w-full'>
            <div className="text-36px">Portfolio</div>
            <button onClick={()=>setOpen(!open)}><MenuIcon className="w-12 md:hidden"/></button>
        </div>
        <div className={`flex gap-2 items-center flex-col my-auto md:my-none md:flex-row ${open ? '' : 'hidden'} md:flex`}>
            <MenuItem href='/'>Home</MenuItem>
            <MenuItem href='/skills'>Skills</MenuItem>
            <MenuItem href='/contact'>Contact</MenuItem>
        </div>
    </nav>
}
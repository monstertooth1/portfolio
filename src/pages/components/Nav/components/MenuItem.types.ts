export interface MenuItemProps{
    href: string,
    children: string
}
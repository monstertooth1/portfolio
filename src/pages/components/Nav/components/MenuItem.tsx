import React from 'react';
import {MenuItemProps} from "./MenuItem.types";
import {Link} from "wouter";
import useRouteAssets from "../../../../hooks/useRouteAssets";

function MenuItem({href, children}:MenuItemProps) {
    const assets = useRouteAssets();
    return <Link href={href} className={`transition duration-page-flip delay-page-flip ${assets.location === href ? assets.text+' text-36px' : ''}`}>{children}</Link>;
}

export default MenuItem;